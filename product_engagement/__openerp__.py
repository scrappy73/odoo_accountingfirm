# -*- coding: utf-8 -*-
{
    'name': "Product Engagement",

    'summary': """
        Adds Engagement field to the Products Model.""",

    'description': """
        This model adds an engagement description to the PRoducts model.
        The engagement description is editable in a new page on the Products form view.
        
    """,

    'author': "Craig Nunemaker",
    'website': "http://www.nunemakerllc.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Specific Industry Applications',
    'version': '0.2',

    # any module necessary for this one to work correctly
    'depends': ['base','product','sale',],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}