#!/bin/bash

function update_git_repo {
        if [-d "~/DOWNLOAD/$1" ]; then
                echo "No files exist for update_git_repo"
                sleep 10
        else
		echo "============================================================"
                echo "Running update_git_repo"
                cd ~/DOWNLOAD/$1/
                git pull --verbose
                cd ~/
		if [ -f "~/DOWNLOAD/$1/__init__.py" ]; then
			ln --verbose -s ~/DOWNLOAD/$1 -t ~/addons/
                else	
			ln --verbose -s ~/DOWNLOAD/$1/* -t ~/addons/
                fi
		rm ~/addons/__unported__ --verbose
        fi
}

function update_odoo_git {
        cd ~/
	echo "============================================================"
        echo "running update_odoo_git"
        git pull --verbose
}

function update_odoo_DB {
        cd ~/
	echo "============================================================"
        echo "Updating the database. this may take a while. For update check odoo log on separate terminal"
	echo $1
        ./openerp-server \
                --config=/etc/odoo-server.conf \
                --update=all \
                --stop-after-init \
                --database=$1
}

function backup_odoo {
        cd ~/BACKUP/
        tar -cvzf ~/BACKUP/$(date +"%m%d").tgz ~/a* ~/s* ~/o* ~/M* ~/R* ~/r* ~/s* ~/d*
}

backup_odoo
update_odoo_git
update_git_repo account-financial-reporting
update_git_repo server-tools
update_git_repo sale-workflow
update_git_repo account-financial-tools
update_git_repo purchase-workflow
update_git_repo stock-logistics-transport
update_git_repo web
update_git_repo reporting-engine
update_git_repo project-service
update_git_repo product-attribute
update_git_repo pos
update_git_repo partner-contact
update_git_repo crm
update_git_repo odoo-grupoesoc-addons
update_git_repo knowledge
update_git_repo web_m2x_options
update_git_repo odoo_accountingfirm
update_odoo_DB cafio
#update_odoo_DB dannys
#update_odoo_DB cafio_test
update_odoo_DB ezPawn
exit
